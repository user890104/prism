## 0.6 (2014-04-13)

* added waitpid() to prevent spawning multiple children in static mode

## 0.5 (2013-04-23)

* first release on GitHub
